import java.util.ArrayList;


public class Package {
		private String name;
		private ArrayList<String> dependencies;
		
		public Package(String fileLine) throws Exception{
			
			//Make sure that the line is valid (contains '->'
			if(!fileLine.contains("->") && !fileLine.equals("")){
				throw new Exception("Line is not valid");
			}
			
			
			String[] parts = fileLine.split("->");
			name = parts[0].trim();
			
			dependencies = new ArrayList<String>();

			//Check if the package actually has any dependencies
			if(parts.length == 2){
				String[] deps = parts[1].split(" ");
				
				for(String d : deps){
					//Check if the dependency name is not empty or whitespace
					if(!(d.isEmpty() || d.trim().equals(""))){
						dependencies.add(d.trim());
					}
					
				}
			}
		}
		
		public String getName(){
			return name;
		}
		
		public ArrayList<String> getDependencies(){
			return dependencies;
		}
		
}
