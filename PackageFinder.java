import java.io.BufferedReader;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;


public class PackageFinder {

	
	public static ArrayList<Package> packages;
	
	public static void main(String[] args) {
		
		//Check if there are arguments passed in
		if(args.length == 0){
			System.out.println("Error: No arguments entered.");
			System.exit(1);
		}
		
		packages = new ArrayList<Package>();
		
		try{
			BufferedReader b = new BufferedReader(new FileReader(args[0]));
			String line;
			while((line = b.readLine()) != null){
				//Skip blank lines
				if(!(line.isEmpty() || line.trim().equals("") || line.trim().equals("\n"))){
					packages.add(new Package(line));
				}
				
			}
		}catch(Exception e){
			System.out.printf("Error: %s\n", e.toString());
			System.exit(1);
		}
		
		for(int i = 1; i < args.length; i++){
			System.out.printf("%s -> ", args[i]);
			for(String d : findAllDependencies(args[i])){
					System.out.printf("%s ", d);
			}
			System.out.println();
		}

	}
	
	public static ArrayList<String> findAllDependencies(String name){
		ArrayList<String> deps = new ArrayList<String>();

		//Check if the package is in the list
		Package p;
		int idx = packageLocation(name);
		if(idx == -1){
			return deps;
		}
		p = packages.get(idx);
		
		//find the primary dependencies
		for(String s : p.getDependencies()){
			deps.add(s);
		}
		
		for(int i = 0; i < deps.size(); i++){
			int j;
			if((j = packageLocation(deps.get(i))) != -1){
				for(String s : packages.get(j).getDependencies()){
					if(deps.indexOf(s) == -1){
						deps.add(s);
					}
				}
			}
		}
		
		//Make sure that a package does not appear to depend on itself
		if(deps.indexOf(name) != -1){
			deps.remove(deps.indexOf(name));
		}
		
		//Order the list alphabetically
		Collections.sort(deps);
		
		return deps;
	}
	
	
	//Returns the index of the package with name in package arraylist, or -1 if not present;
	public static int packageLocation(String name){
		int i = 0;
		
		for(i = 0; i < packages.size(); i++){
			if(packages.get(i).getName().equals(name)){
				return i;
			}
		}
		i = -1;
		
		return i;
	}

}
